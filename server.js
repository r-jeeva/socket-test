require('dotenv').config()

var app = require('express')()
var server = require('http').Server(app)
var io = require('socket.io')(server)

server.listen(process.env.PORT)
// WARNING: app.listen(80) will NOT work here!

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/client.html')
})

const sockets = []
io.of('chat').on('connection', function (socket) {
  sockets.push(socket.id)
  console.log('chat connected')
  socket.join('group')
  console.log('group joined')
  socket.on('message', function (data) {
    console.log(data)
    socket.to('group').emit('message', data)
  })
  // socket.emit('message', 'server message')
  socket.on('disconnect', function () {
    sockets.splice(sockets.indexOf(socket.id), 1)
    console.log('chat disconnected')
  })
})
